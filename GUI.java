import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class GUI {

	private JFrame frame;
	private JTable table_1;
	Clients c=new Clients();
	Bank b=new Bank();
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JScrollPane scrollPane;
	private JTable table;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JButton btnAdaugaCont;
	private JButton btnStergeCont;
	private JLabel lblAdaugaextrageBaniIn;
	private JTextField textField_8;
	private JTextField textField_9;
	private JLabel lblSuma1;
	private JLabel lblIdCont;
	private JButton btnAdauga;
	private JButton btnExtrage;
	private JButton btnUpdate;
	private JButton btnUpdatecont;
	private JTextField textField_10;
	private JTextField textField_11;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 750, 550);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setTitle("BankManagement--Rares Cristea");
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 248, 380, 221);
		frame.getContentPane().add(scrollPane_1);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 15, 380, 211);
		frame.getContentPane().add(scrollPane);
		
		table = new JTable();
		DefaultTableModel tableModel=new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"ID", "Nume", "CI", "Varsta"
			}
		);
		table.setModel(tableModel);
		scrollPane.setViewportView(table);
		
		table.setBackground(Color.cyan);
		table_1 = new JTable();
		DefaultTableModel tableModel1=new DefaultTableModel(
			new Object[][] {
				
			},
			new String[] {
				"ID_Cont", "NumeClient", "Data", "Suma"
			}
		);
		table_1.setModel(tableModel1);
		scrollPane_1.setViewportView(table_1);
		table_1.setBackground(Color.cyan);
		
		textField = new JTextField();
		textField.setBounds(473, 19, 86, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblId = new JLabel("ID");
		lblId.setBounds(400, 18, 46, 14);
		frame.getContentPane().add(lblId);
		
		textField_1 = new JTextField();
		textField_1.setBounds(473, 46, 86, 20);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblNume = new JLabel("Nume");
		lblNume.setBounds(400, 49, 46, 14);
		frame.getContentPane().add(lblNume);
		
		textField_2 = new JTextField();
		textField_2.setBounds(473, 77, 86, 20);
		frame.getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		JLabel lblCi = new JLabel("CI");
		lblCi.setBounds(400, 80, 46, 14);
		frame.getContentPane().add(lblCi);
		
		textField_3 = new JTextField();
		textField_3.setBounds(473, 108, 86, 20);
		frame.getContentPane().add(textField_3);
		textField_3.setColumns(10);
		
		JLabel lblVarsta = new JLabel("Varsta");
		lblVarsta.setBounds(400, 111, 46, 14);
		frame.getContentPane().add(lblVarsta);
		
		textField_4 = new JTextField();
		textField_4.setBounds(473, 275, 86, 20);
		frame.getContentPane().add(textField_4);
		textField_4.setColumns(10);
		
		JLabel lblIdcont = new JLabel("ID_Cont");
		lblIdcont.setBounds(400, 278, 46, 14);
		frame.getContentPane().add(lblIdcont);
		
		textField_5 = new JTextField();
		textField_5.setBounds(473, 306, 86, 20);
		frame.getContentPane().add(textField_5);
		textField_5.setColumns(10);
		
		JLabel lblNumeclient = new JLabel("NumeClient");
		lblNumeclient.setBounds(400, 309, 68, 14);
		frame.getContentPane().add(lblNumeclient);
		
		textField_6 = new JTextField();
		textField_6.setBounds(473, 337, 86, 20);
		frame.getContentPane().add(textField_6);
		textField_6.setColumns(10);
		
		JLabel lblData = new JLabel("Data");
		lblData.setBounds(400, 340, 46, 14);
		frame.getContentPane().add(lblData);
		
		textField_7 = new JTextField();
		textField_7.setBounds(473, 368, 86, 20);
		frame.getContentPane().add(textField_7);
		textField_7.setColumns(10);
		
		JLabel lblSuma = new JLabel("Suma");
		lblSuma.setBounds(400, 371, 46, 14);
		frame.getContentPane().add(lblSuma);
		
		JLabel lblPlatesteMonetar = new JLabel("Plateste Monetar");
		lblPlatesteMonetar.setBounds(400, 136, 159, 14);
		frame.getContentPane().add(lblPlatesteMonetar);
		
		JLabel lblSuma_1 = new JLabel("Suma");
		lblSuma_1.setBounds(400, 161, 46, 14);
		frame.getContentPane().add(lblSuma_1);
		
		JLabel lblIdCont_1 = new JLabel("ID Cont");
		lblIdCont_1.setBounds(400, 186, 46, 14);
		frame.getContentPane().add(lblIdCont_1);
		
		textField_10 = new JTextField();
		textField_10.setBounds(473, 158, 86, 20);
		frame.getContentPane().add(textField_10);
		textField_10.setColumns(10);
		
		textField_11 = new JTextField();
		textField_11.setBounds(473, 183, 86, 20);
		frame.getContentPane().add(textField_11);
		textField_11.setColumns(10);
		
		
		JButton btnAdaugaclient = new JButton("Adauga Client");
		btnAdaugaclient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tableModel.setRowCount(0);
				c.adaugaClient(new Person(textField.getText(),textField_1.getText(),textField_2.getText(),Integer.parseInt(textField_3.getText())));	
				c.readFile();
				for(final Person p:c.getClient()){
					tableModel.addRow(new Object[]{p.getID(),p.getName(),p.getCI(),p.getVarsta()});
				    c.writeFile();
				}
									
				}
			
		});
		btnAdaugaclient.setBounds(593, 28, 114, 23);
		frame.getContentPane().add(btnAdaugaclient);
		
		JButton btnStergeclient = new JButton("Sterge Client");
		btnStergeclient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			int i=table.getSelectedRow();
			if(i>=0){
				tableModel.removeRow(i);
			}
			else{
				System.out.println("Delete error");
			}
			}
		});
		btnStergeclient.setBounds(593, 62, 114, 23);
		frame.getContentPane().add(btnStergeclient);
		
		btnAdaugaCont = new JButton("Adauga Cont");
		btnAdaugaCont.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tableModel1.setRowCount(0);
				b.adaugaCont(new Account(textField_4.getText(),textField_5.getText(),textField_6.getText(),Integer.parseInt(textField_7.getText())));
				b.readFile();
				for(final Account c:b.getCont()){
					tableModel1.addRow(new Object[]{c.getID(),c.getNumeClient(),c.getData(),c.getSuma()});
					b.writeFile();
					
				}
			}
		});
		btnAdaugaCont.setBounds(593, 274, 114, 23);
		frame.getContentPane().add(btnAdaugaCont);
		
		btnStergeCont = new JButton("Sterge Cont");
		btnStergeCont.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int i=table_1.getSelectedRow();
				if(i>=0){
					tableModel1.removeRow(i);
				}
				else{
					System.out.println("Delete error");
				}
			}
		});
		btnStergeCont.setBounds(593, 305, 114, 23);
		frame.getContentPane().add(btnStergeCont);
		
		lblAdaugaextrageBaniIn = new JLabel("Adauga/Extrage bani in cont");
		lblAdaugaextrageBaniIn.setBounds(400, 396, 159, 14);
		frame.getContentPane().add(lblAdaugaextrageBaniIn);
		
		textField_8 = new JTextField();
		textField_8.setBounds(473, 421, 86, 20);
		frame.getContentPane().add(textField_8);
		textField_8.setColumns(10);
		
		textField_9 = new JTextField();
		textField_9.setBounds(473, 452, 86, 20);
		frame.getContentPane().add(textField_9);
		textField_9.setColumns(10);
		
		lblSuma1 = new JLabel("Suma");
		lblSuma1.setBounds(400, 424, 68, 14);
		frame.getContentPane().add(lblSuma1);
		
		lblIdCont = new JLabel("ID Cont");
		lblIdCont.setBounds(400, 455, 68, 14);
		frame.getContentPane().add(lblIdCont);
		
		btnAdauga = new JButton("Adauga");
		btnAdauga.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int a=Integer.parseInt(textField_8.getText());
				String d=textField_9.getText();
				int i=0;
				for(final Account c:b.getCont()){
				    String f=c.getID();
				    int g=c.getSuma();
				    if(f.equals(d)){
				    g+=a;
				    tableModel1.setValueAt(g, i, 3);
				    }
				    i++;	

			}
		}
		});
		btnAdauga.setBounds(593, 420, 114, 23);
		frame.getContentPane().add(btnAdauga);
		
		btnExtrage = new JButton("Extrage");
		btnExtrage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int a=Integer.parseInt(textField_8.getText());
				String d=textField_9.getText();
				int i=0;
				for(final Account c:b.getCont()){
				    String f=c.getID();
				    int g=c.getSuma();
				    if(f.equals(d)){
				    g-=a;
				    tableModel1.setValueAt(g, i, 3);
				    }
				    i++;	

			}
			}
		});
		btnExtrage.setBounds(593, 451, 114, 23);
		frame.getContentPane().add(btnExtrage);
		
		btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int i=table.getSelectedRow();
				tableModel.setValueAt(textField.getText(), i, 0);
				tableModel.setValueAt(textField_1.getText(), i, 1);
				tableModel.setValueAt(textField_2.getText(), i, 2);
				tableModel.setValueAt(textField_3.getText(), i, 3);
			
			}
		});
		btnUpdate.setBounds(593, 96, 114, 23);
		frame.getContentPane().add(btnUpdate);
		
		btnUpdatecont = new JButton("UpdateCont");
		btnUpdatecont.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int i=table_1.getSelectedRow();
				tableModel1.setValueAt(textField_4.getText(), i, 0);
				tableModel1.setValueAt(textField_5.getText(), i, 1);
				tableModel1.setValueAt(textField_6.getText(), i, 2);
				tableModel1.setValueAt(textField_7.getText(), i, 3);
			}
		});
		btnUpdatecont.setBounds(593, 336, 114, 23);
		frame.getContentPane().add(btnUpdatecont);
		
		JButton btnPopuleazaTabele = new JButton("Populeaza Tabelele");
		btnPopuleazaTabele.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				c.readFile();
				b.readFile();
				tableModel.setRowCount(0);
				tableModel1.setRowCount(0);
				for(final Person p:c.getClient()){
					tableModel.addRow(new Object[]{p.getID(),p.getName(),p.getCI(),p.getVarsta()});
				}
				for(final Account a:b.getCont()){
					tableModel1.addRow(new Object[]{a.getID(),a.getNumeClient(),a.getData(),a.getSuma()});
				}
			}
		});
		btnPopuleazaTabele.setBounds(20, 480, 159, 23);
		frame.getContentPane().add(btnPopuleazaTabele);
		
		
		
		JButton btnPlateste = new JButton("Plateste");
		btnPlateste.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int a=Integer.parseInt(textField_10.getText());
				String d=textField_11.getText();
				int i=0;
				for(final Account c:b.getCont()){
				    String f=c.getID();
				    int g=c.getSuma();
				    if(f.equals(d)){
				    g-=a;
				    tableModel1.setValueAt(g, i, 3);
				    }
				    i++;	

			}
			}
		});
		btnPlateste.setBounds(593, 161, 114, 23);
		frame.getContentPane().add(btnPlateste);
		
		
		
	}
}
