import java.io.Serializable;

public class Person implements Serializable,Comparable<Person> {
    
    private String ID;
    private String name;
    private String CI;
    private int varsta;
    
    public Person(String i,String nume,String c, int v){
    	
    	ID=i;
    	name=nume;
    	CI=c;
    	varsta=v;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}

	public String getCI() {
		return CI;
	}

	public void setCI(String cI) {
		CI = cI;
	}

	public int getVarsta() {
		return varsta;
	}

	public void setVarsta(int varsta) {
		this.varsta = varsta;
	}
    
	@Override
	public int compareTo(Person persoana) {
		return ID.compareTo(persoana.ID);
		
	}
}
