import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Iterator;
import java.util.TreeSet;

public class Bank implements Serializable {
  TreeSet<Account> cont;
  String fileName;

  public Bank(){
	  this.fileName="accounts.bin";
	  this.cont=new TreeSet<Account>();
	  Account a=new Account("1","Rares","13.06.2010",2000);
	  cont.add(a);
  }
public TreeSet<Account> getCont() {
	return cont;
}


public void setCont(TreeSet<Account> cont) {
	this.cont = cont;
}


public void adaugaCont(Account c){
	cont.add(c);
	
}

public void stergeCont(Account c){
	cont.remove(c);
}

public void writeFile(){
	
	try{
	ObjectOutputStream os=new ObjectOutputStream(new FileOutputStream(fileName));
	Iterator<Account> i=cont.iterator();
	
	while(i.hasNext()){
		os.writeObject(i.next());
		System.out.println("Am scris in " + fileName);
	}
	os.close();
	} catch (IOException i) {
		   i.printStackTrace();
		   
		  }
	}
public void readFile() 
{
	 System.out.println("Citim din " + fileName);
	  try {
	   ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName));
	   Account p = null;
	   try {
	    try {
	     while ((p =(Account)in.readObject()) != null) 
	     {		    	 
	       this.adaugaCont(p);
	     }
	     in.close();
	    } catch (EOFException e) {

	    }
	   } catch (ClassNotFoundException e1) {
	    System.out.println("acuma dam fail");
	    e1.printStackTrace();
	   }
	  } catch (IOException i) {
	   System.out.println("Product class not found");
	   i.printStackTrace();
	   return;
	  }

	 }
}

