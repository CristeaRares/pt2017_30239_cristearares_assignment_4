import java.io.Serializable;

public class Account implements Serializable,Comparable<Account> {
   private String ID;
   private String NumeClient;
   private String Data;
   private int suma;
   
   public Account(String i,String nume,String d,int s){
	   ID=i;
	   NumeClient=nume;
	   Data=d;
	   suma=s;
   }

public String getID() {
	return ID;
}

public void setID(String iD) {
	ID = iD;
}

public String getNumeClient() {
	return NumeClient;
}

public void setNumeClient(String numeClient) {
	NumeClient = numeClient;
}

public String getData() {
	return Data;
}

public void setData(String data) {
	Data = data;
}

public int getSuma() {
	return suma;
}

public void setSuma(int suma) {
	this.suma = suma;
}

@Override
public int compareTo(Account cont) {
	return ID.compareTo(cont.ID);
	
}
}
