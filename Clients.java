
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Iterator;
import java.util.TreeSet;

public class Clients implements Serializable{
  TreeSet<Person> client;
  private String fileName;

  
  public Clients(){
	  this.fileName="clients.bin";
	  this.client=new TreeSet<Person>();
	  Person p=new Person("1","Rares","12",21);
	  client.add(p);
  }
  
  public void adaugaClient(Person p){
	  client.add(p);
  }

  public void stergeClient(Person p){
	  client.remove(p);
  }


public TreeSet<Person> getClient() {
	return client;
}

public void setClient(TreeSet<Person> client) {
	this.client = client;
}

public String getFileName() {
	return fileName;
}

public void setFileName(String fileName) {
	this.fileName = fileName;
}

public void writeFile(){
	
	try{
	ObjectOutputStream os=new ObjectOutputStream(new FileOutputStream(fileName));
	Iterator<Person> i=client.iterator();
	
	while(i.hasNext()){
		os.writeObject(i.next());
		System.out.println("Am scris in " + fileName);
	}
	os.close();
	} catch (IOException i) {
		   i.printStackTrace();
		   
		  }
	}
public void readFile() 
{
	 System.out.println("Citim din " + fileName);
	  try {
	   ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName));
	   Person p = null;
	   try {
	    try {
	     while ((p =(Person)in.readObject()) != null) 
	     {		    	 
	       this.adaugaClient(p);
	     }
	     in.close();
	    } catch (EOFException e) {

	    }
	   } catch (ClassNotFoundException e1) {
	    System.out.println("acuma dam fail");
	    e1.printStackTrace();
	   }
	  } catch (IOException i) {
	   System.out.println("Product class not found");
	   i.printStackTrace();
	   return;
	  }

	 }
}

